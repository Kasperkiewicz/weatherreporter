package com.example.WeatherReporter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.json.JSONObject;

@SpringBootApplication
public class WeatherReporterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherReporterApplication.class, args);
	}

}
