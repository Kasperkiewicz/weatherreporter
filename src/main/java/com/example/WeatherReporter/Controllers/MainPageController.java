package com.example.WeatherReporter.Controllers;

import com.example.WeatherReporter.Services.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainPageController {

    @Autowired
    SettingsService settingsService;

    @GetMapping("/")
    public String getMainPage(Model model){
       // JSONObject obj = new JSONObject();
        String value = settingsService.sendRequestToAirly();
        model.addAttribute("text", value);
        return "index";
    }
}
