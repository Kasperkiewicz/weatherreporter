package com.example.WeatherReporter.Services;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class SettingsService {

   final private String airlyKey = "ubOAzDTRjfGRmPJeEkKpE6gXyK6g8JkA";
   private boolean logged;
   private boolean supervisor;
   private List<Locale> languages = Arrays.asList(new Locale("en"), new Locale("pl"));

   public String getAirlyKey(){
       return airlyKey;
   }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public boolean isLogged(){
       return logged;
    }

    public void setSupervisor(boolean supervisor){
       this.supervisor = supervisor;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public String sendRequestToAirly(){
       String requestAdress = "https://airapi.airly.eu/v2/measurements/nearest?indexType=AIRLY_CAQI&lat=50.21240&lng=19.25580&maxDistanceKM=3";
       //50.21240,19.25580,i756
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setAcceptLanguageAsLocales(languages);
        headers.set("apikey",airlyKey);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange(requestAdress, HttpMethod.GET, entity, String.class);

       return response.toString();
       /*
       		JSONObject jsonObject = new JSONObject();
		jsonObject.put("city", "Jaworzno");
		jsonObject.put("country","Poland");
		System.out.println(jsonObject.toString());


		WeatherStation weatherStation;
		if(jsonObject.has("city") && jsonObject.has("country")){
			weatherStation = new WeatherStation(jsonObject.getString("city"), jsonObject.getString("country"));
			System.out.println(weatherStation);
		}
        */
    }
}
