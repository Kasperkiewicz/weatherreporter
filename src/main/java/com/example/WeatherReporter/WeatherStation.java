package com.example.WeatherReporter;

public class WeatherStation {

    private String city;
    private String country;

    public void setCity(String city){
        this.city = city;
    }

    public String getCity(){
        return city;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getCountry(){
        return country;
    }

    public WeatherStation(String city, String country){
        this.city = city;
        this.country = country;
    }

    public WeatherStation(){
        city = "City";
        country = "Country";
    }

    @Override
    public String toString() {
        return city + " , " + country;
    }
}
