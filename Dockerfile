FROM java:8
LABEL maintainer = "sz.kasperkiewicz@gmail.com"
EXPOSE 8090
ADD target/WeatherReporter-0.0.1-SNAPSHOT.jar WeatherReporter-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","WeatherReporter-0.0.1-SNAPSHOT.jar"]